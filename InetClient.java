/*------------------------------------------------------------------------------------
	File is: InetClient.java, Version 1.3

	Use with InetServer.java, the multithreaded listener.

	This will not run unless TCP/IP is loaded on your machine.

	1. Andrew Bell / 01-10-2013

	2. To compile this server for to test connection you need to
	   	   enter the following command in the terminal:

	   $ javac InetServer.java  // This will producue the server InetServer
	   $ javac InetClient.java // This will produce the client, InetCliet

	3. Then to get the server running we just run the following in a shell:

   		$ java InetServer
	    You should see the following output when it's running correctly and waiting for
        clients to connect
	    "Clark Elliot's Inet server starting up, listening at port 1565."

		In separate shell(s) run:
		$ java InetClient

        #####
        ##### see comments.html for testing console output
        #####

    4. Java version used:   java version "1.7.0_10"
                            Java(TM) SE Runtime Environment (build 1.7.0_10-b18)
                            Java HotSpot(TM) 64-Bit Server VM (build 23.6-b04, mixed mode)


    5. List of files needed for running the program.

        Required files for this initial turn in are:
            a. checklist.html
            b. InetClient.java
            c. InetServer.java
            d. comments.html - Included for easier to read format for outputs


	6. Notes:

        e.g.:
		Note that there is a minor design flaw, which is interesting from a pedagocial
        standpoint.  Uncomment the Thread.sleep line in the main server listening loop to
        have the flaw illustrated.

        This was the resulting messsage received when Thread.sleep was uncommented:

        ^X^C10:35:23 ~/Dropbox/depaul/CSC435/Homeworks/InetServer$ javac InetServer.java
        InetServer.java:133: error: <identifier> expected
                    try(Thread.sleep(10000);} catch (InterruptedException ex) {}
                                    ^
        InetServer.java:133: error: ')' expected
                    try(Thread.sleep(10000);} catch (InterruptedException ex) {}
                                     ^
        InetServer.java:133: error: '{' expected
                    try(Thread.sleep(10000);} catch (InterruptedException ex) {}
                                          ^
        3 errors
------------------------------------------------------------------------------------*/
import java.io.*;   // Get the Input Output libraries
import java.net.*;  // Get the Java networking libraries

public class InetClient {
	public static void main(String args[]) {
		String serverName;

        // If no argument is passed, the client connects to localhost
        if(args.length < 1) {
			serverName = "localhost";
		}
        // Server is passed as argument / serverName set to args[0]
		else {
			serverName = args[0];
		}
		System.out.println("Clark Elliott's Inet Client.\n");

		/* PrintLocalAddress(); */
		System.out.println("Using server: " + serverName + ", Port: 8888");
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		try {
			String name;
			do {
				System.out.print("Enter a hostname or an IP address, (quit) to end: ");
				System.out.flush();
				name = in.readLine();
				if(name.indexOf("quit") < 0) {
					getRemoteAddress(name, serverName);
				}
			} while(name.indexOf("quit") < 0);
			System.out.println("Cancelled by user request.");
		} catch(IOException x) {
			x.printStackTrace();
            // Data reveived from server is pushed to stack for debugging
		}
	}

    // Prints local IP if connection fails
	static void PrintLocalAddress() {
		try {
			InetAddress me = InetAddress.getLocalHost();
			System.out.println("My local name is:       " + me.getHostName());
			System.out.println("My local IP address is: " + toText(me.getAddress()));
		} catch(UnknownHostException x) {
			System.out.println("I appear to be unknown to myself.  Firewall?:");
		}
	}

	static String toText(byte ip[]) {   /* Make portable for 128 bit format */
		StringBuffer result = new StringBuffer();
		for(int i = 0; i < ip.length; ++ i) {
			if(i > 0) {
				result.append(".");
			}
			result.append(0xFF & ip[i]);
		}
		return result.toString();
	}

	static void getRemoteAddress(String name, String serverName) {
		Socket sock;
		BufferedReader fromServer;
		PrintStream toServer;
		String textFromServer;

		try {
			/* Open our connection to server port, choose your own port number.. */
			sock = new Socket(serverName, 8888);

			// Create filter I/O streams for the socket:
			fromServer = new BufferedReader(new InputStreamReader(sock.getInputStream()));
			toServer = new PrintStream(sock.getOutputStream());

			// Send machine name or IP address to server:
			toServer.println(name);
			toServer.flush();

			// Read two or three lines of response from the server,
			// and block while synchronously waiting:
            // Only reads two lines if hostname checking does not return or support IPv6
			for(int i = 1; i <= 3; i++) {
				textFromServer = fromServer.readLine();
				if(textFromServer != null) {
					System.out.println(textFromServer);
				}
			}
			sock.close();
		} catch(IOException x) {
			System.out.println("Socket error.");
			x.printStackTrace();
            // Data reveived from server is pushed to stack for debugging
		}
	}
}