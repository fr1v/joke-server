/*------------------------------------------------------------------------------------
    File is: InetServer.java, Version 1.3

    A multithreaded server for InetClient
    Elliott, after Hughes, Shoffner, Winslow
    This will not run unless TCP/IP is loaded on your machine.


	1. Andrew Bell / 01-10-2013

	2. To compile this server for to test connection you need to
	   	   enter the following command in the terminal:

	   $ javac InetServer.java  // This will producue the server InetServer
	   $ javac InetClient.java // This will produce the client, InetCliet

	3. Then to get the server running we just run the following in a shell:

   		$ java InetServer
	    You should see the following output when it's running correctly and waiting for    
        clients to connect
	    "Clark Elliot's Inet server starting up, listening at port 1565."

		In separate shell(s) run:
		$ java InetClient

        #####
        ##### see comments.html for testing console output
        ##### 

    4. Java version used:   java version "1.7.0_10"
                            Java(TM) SE Runtime Environment (build 1.7.0_10-b18)
                            Java HotSpot(TM) 64-Bit Server VM (build 23.6-b04, mixed mode)

    
        5. List of files needed for running the program.
    
        Required files for this initial turn in are:
            a. checklist.html
            b. InetClient.java
            c. InetServer.java
            d. comments.html - Included for easier to read format for outputs


	6. Notes:
		
        e.g.:
		Note that there is a minor design flaw, which is interesting from a pedagocial  
        standpoint.  Uncomment the Thread.sleep line in the main server listening loop to   
        have the flaw illustrated.
        
        This was the resulting messsage received when Thread.sleep was uncommented:
        
        ^X^C10:35:23 ~/Dropbox/depaul/CSC435/Homeworks/InetServer$ javac InetServer.java
        InetServer.java:133: error: <identifier> expected
                    try(Thread.sleep(10000);} catch (InterruptedException ex) {}
                                    ^
        InetServer.java:133: error: ')' expected
                    try(Thread.sleep(10000);} catch (InterruptedException ex) {}
                                     ^
        InetServer.java:133: error: '{' expected
                    try(Thread.sleep(10000);} catch (InterruptedException ex) {}
                                          ^
        3 errors
        
------------------------------------------------------------------------------------*/
// Here the libraries needed to run the program are downloaded.  This progarm
// would not compile or run without the correct libraries being used.
import java.io.*; // Get the Input Output libraries
import java.net.*; // Get the Java networking libraries

// Worker class starts a socket here which is where the communication between the client/server will take place.
class Worker extends Thread {       // Class definition
    Socket sock;                    // Class member, socket, local to Worker.
    Worker(Socket s) {sock = s;}   // Constructor, assign arg s to local sock
    // Client is now connected local socket


    @Override
    public void run() {
        // Get I/O streams from the socket:
        PrintStream out = null;
        BufferedReader in = null;
        try {
            out = new PrintStream(sock.getOutputStream());
            in = new BufferedReader(new InputStreamReader(sock.getInputStream()));

            // controlSwitch should have been initialized to 
            // Note that it is remotely possible that this branch will not execute:
            if (InetServer.controlSwitch != true) {
                System.out.println("Listener is now shutting down as per client request.");
                out.println("Server is now shutting down.  Goodbye!");
            }
        else try {
            // Thread pauses here to wait for client's command:
            // Should receive an ip address to look up or a shutdown request.
            // Any other answer will be handled by I/O exceptions.
            String name;
            name = in.readLine();

            if (name.indexOf("shutdown") > -1) {
                InetServer.controlSwitch = false;
                System.out.println("Worker has captured a shutdown request.");
                out.println("Shutdown request has been noted by worker.");
                out.println("Please send final shutdown request to listener.");
            }
            else {
                System.out.println("Looking up " + name);
                printRemoteAddress(name, out);
            }
        } catch(IOException x) {
            System.out.println("Server read error");
            x.printStackTrace();
            // The client's input command is pushed on the stack, can be used for debugging.
        }
        sock.close(); // close this connection, but not the server
        } catch(IOException ioe) {System.out.println(ioe);}
    }

    // Server receives the hostname given by client, and searches for it's IP addess.
    static void printRemoteAddress(String name, PrintStream out) {
        try {
            out.println("Looking up " + name + "...");
            InetAddress machine = InetAddress.getByName(name);
            out.println("Host name : " + machine.getHostName());
            out.println("Host IP : " + toText(machine.getAddress()));
        } catch(UnknownHostException ex) {
            out.println("Failed in attempt to look up " + name);
        }
    }

    // Function which converts IPv4 address to IPv6
    static String toText(byte ip[]) { /* Make portable for 128 bit format */
            StringBuffer result = new StringBuffer();
            for(int i = 0; i < ip.length; ++ i) {
                if (i > 0) {
                    result.append(".");
                }
                result.append(0xFF & ip[i]);
            }
            return result.toString();
    }
}

public class InetServer {
    public static boolean controlSwitch = true;

    public static void main(String a[]) throws IOException {
        
        // Set to 6 because can not be any more simultanious connections that, although very unlikely.
        int q_len = 6; /* Number of requests for OpSys to queue */
        int port = 8888;  // Ran a couple examples at port 1565, changed to 8888 to test. - successful
        Socket sock;

        // Opens a server socket which is where the communication between the server and client will take place.
        ServerSocket servsock = new ServerSocket(port, q_len);

        System.out.println("Clark Elliot's Inet server starting up, listening at port " + port + ".\n");
        while(controlSwitch) {
            
            // Server waiting here for a client to connect to its IP:port
            sock = servsock.accept();
            
            // If client reaches correct IP & Port connection automatically accepted:
            // Worker class is called:
            new Worker(sock).start(); // Uncomment to see shutdown bug:
            //try(Thread.sleep(10000);} catch (InterruptedException ex) {}
        }
    }
}